package comsamueHCB.CovidAlertservice.service;

import comsamueHCB.CovidAlertservice.dto.AlertStatus;
import comsamueHCB.CovidAlertservice.dto.StateData;
import comsamueHCB.CovidAlertservice.dto.SummaryData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AlertService {
    @Autowired
    private Covid19Data covid19Data;

    public AlertStatus getAlertAboutState(String state) {
        AlertStatus alertStatus = new AlertStatus();
        StateData stateData = covid19Data.getStateData(state);
        alertStatus.setStateData(stateData);
        if (stateData.getTotalConfirmed() < 1000) {
            alertStatus.setAlertLevel("GREEN");
            alertStatus.setMeasuresToBeTakenBy((List.of("NO PANICK")));
        } else if (stateData.getTotalConfirmed() > 1000 && stateData.getTotalConfirmed() < 10000) {
            alertStatus.setAlertLevel("ORANGE");
            alertStatus.setMeasuresToBeTakenBy(List.of("ONLY ESSENTIAL SERVICES"));

        } else {
            alertStatus.setAlertLevel("RED");
            alertStatus.setMeasuresToBeTakenBy(List.of("ABSOLUTE LOCKDOWN ,ONLY MEDICAL AND FOOD SERVICES"));

        }
        return alertStatus;
    }

    public SummaryData getOverAllSummary() {
        return covid19Data.getSummaryData("summary");

    }
}
