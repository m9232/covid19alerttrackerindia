package comsamueHCB.CovidAlertservice.dto;

import java.util.List;

public class AlertStatus {
    String alertLevel;
    List<String> measuresToBeTakenBy ;
    private StateData stateData;

    public StateData getStateData() {
        return stateData;
    }

    public void setStateData(StateData stateData) {
        this.stateData = stateData;
    }

    public String getAlertLevel() {
        return alertLevel;
    }

    public void setAlertLevel(String alertLevel) {
        this.alertLevel = alertLevel;
    }

    public List<String> getMeasuresToBeTakenBy() {
        return measuresToBeTakenBy;
    }

    public void setMeasuresToBeTakenBy(List<String> measuresToBeTakenBy) {
        this.measuresToBeTakenBy = measuresToBeTakenBy;
    }
}
