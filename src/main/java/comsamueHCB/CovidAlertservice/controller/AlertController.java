package comsamueHCB.CovidAlertservice.controller;

import comsamueHCB.CovidAlertservice.dto.AlertStatus;
import comsamueHCB.CovidAlertservice.dto.SummaryData;
import comsamueHCB.CovidAlertservice.service.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/india")
public class AlertController {

    @Autowired
    private AlertService alertService;

    @GetMapping("/{state}")
    AlertStatus getAlertStatus(@PathVariable String state) {
        return alertService.getAlertAboutState(state);

    }
    @GetMapping("/summary")
    SummaryData getOverAllSummary(){
        return alertService.getOverAllSummary();
    }

}
